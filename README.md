# Adwaita Icons

App icons for open source and commercial software in Gnome Adwaita style.

## /improved
Improved standard Gnome icons

## /additional
App icons for open source applications

## /commercial
App icons for commercial applications